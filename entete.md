---
title:  "DS - Simulation de compagnie de taxi"
author: Laurent Pierron (Laurent.Pierron@inria.fr)
affiliation: INRIA / Université de Lorraine
tags: [nothing, nothingness]
documentclass: scrartcl
date: 13 Mai 2022
papersize: a4paper
geometry:
  - margin=1.5cm
  - width=18cm
...
